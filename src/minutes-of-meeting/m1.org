#+title: Minutes of Meeting 2018-05-18 
#+AUTHOR: VLEAD
#+DATE: [2018-05-14 Mon]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
Here are the minutes of meeting held on 18th may:

** Duration
From 04:15 to 04:30 

** Attendees
Proff Venkatesh Chopella
Udaypartap
Nishant

** Purpose
- To discuss the experiment content structure developed so far.

** Takeaways
- Got a better understanding on how to go about
implementing our experiment and what exactly each unit
will hold.

** Action
- Will work on common library of linked list.
