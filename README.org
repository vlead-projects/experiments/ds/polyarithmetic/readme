* README FOR POLY ARITHMETIC EXPERIMENT
- Here is the readme for our experiment on Polynomial arithmetic based on our VLEAD project

* Repository Structure 
  #+BEGIN_EXAMPLE
    readme/
    |--- README.org
    |--- init.sh
    |--- makefile
    |--- src/
         |--- index.org
         |--- realization-plan/
         |--- requirements/
         |--- minutes-of-meeting/
         |--- experience/
  #+END_EXAMPLE
